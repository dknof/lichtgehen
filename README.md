# lichtgehen

Ein Programm für den siebten Programmierwettbewerb 2014 vom freiesMagazin, siehe <http://www.freiesmagazin.de/siebter_programmierwettbewerb>

## Kompilieren
einfach 'make' ausführen sollte reichen.

Benötigt wird C++14, getestet ist mit dem Compiler gcc version 4.9.1 und clang++ 3.5.0-6
Alternativ klappt es auch mit dem Compiler gcc version 4.8.3 (siehe Makefile.rules).

In der Datei src/Makefile.rules werden der Compiler sowie die Parameter für den Compiler gesetzt (sofern nicht bereits CXX und CXXFLAGS gesetzt sind).

In der Datei src/Makefile.modules lassen sich vier Module anschalten:
* gtkmm: eine graphische Ausgabe (benötigt die Entwicklerpakete von gtkmm Version 3, debian: libgtkmm-3.0-dev)
* Eingabe: bei der graphischen Ausgabe kann ein Bot gesteuert werden
* Eigenes Spiel: lichtgehen läuft ohne den Spielserver 
Sind die Module gtkmm und Eingabe aktiv, kann über den Spielserver gegen einen eigenen Bot getestet werden, sind alle drei Module aktiv, kann ohne den Spieleserver gegen den Bot gespielt werden.
* Threads: Threadunterstützung, damit der PC auch richtig gefordert wird.
Außerdem wird in der Datei ein Zeitlimit für die Tiefensuche angegeben.

Es werden Raster von maximal 128\*128 Größe unterstützt. Wird mehr benötigt, muss in src/spielraster/raster.h, Zeile 44 das „#define USE\BITSET“ auskommentiert werden, dadurch sinkt allerdings die Performance.

lichtgehen kann auch mit dem Wettbewerbscript mit der graphischen Ausgabe laufen. Das Programm beendet sich allerdings nicht von selber sondern muss dann vom Spielserver hart beendet werden.

## Website

Der Quelltext befindet sich unter <https://gitlab.com/dknof/lichtgehen>.

## Author

* Dr. Diether Knof <dknof+lichtgehen@posteo.de>

## Mitwirken

Lichtgehen ist ein freies Projekt. Bei Interesse kontaktiere den [author](mailto:dknof+lichtgehen@posteo.de).

## Lizenz

Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiter verteilen und/oder modifizieren.

## Status des Projekts

Dieses Projekt ist abgeschlossen (Stand: 10. Oktober 2019).
