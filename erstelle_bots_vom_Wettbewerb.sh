#!/bin/sh

# Holt die Daten from Programmierwettbewerb und erzeugt die Programme.

# 1. Archiv holen
ARCHIV="ftp://ftp.freiesmagazin.de/2014/freiesmagazin-2014-10-contest-complete.tar.gz"
DATEI="freiesmagazin-2014-10-contest-complete.tar.gz"
MD5SUM="8c1f673560e1f54e99a3340dcc38b4f3"
if test -f "$DATEI"; then
  if ! (echo "$MD5SUM $DATEI" | md5sum -c >/dev/null); then
    echo "Datei '$DATEI' ist heruntergeladen, aber passt nicht:"
    md5sum -c "$DATEI"
    echo "$MD5SUM $DATEI" | md5sum -c
    echo "Lösche sie, um sie erneut herunterzuladen."
    rm "$DATEI"
  fi
fi
if test ! -f "$DATEI"; then
  which wget >/dev/null \
    && wget -nd "$ARCHIV"
  test ! -f "$DATEI" \
    && which curl >/dev/null \
    && (curl "$ARCHIV" > "$DATEI")
fi

if test ! -f "$DATEI"; then
  echo "Herunterladen vom Archiv ist fehlgeschlagen."
  echo "Ist wget oder curl installiert?"
  exit
fi
if ! (echo "$MD5SUM $DATEI" | md5sum -c >/dev/null); then
  echo "md5sum passt nicht:"
  echo "$MD5SUM $DATEI" | md5sum -c
  exit
fi


# 2. Archiv entpacken
if test ! -d "freiesmagazin-2014-10-contest"; then
  tar xzf "$DATEI"
fi

# 3. Bots kompilieren
cd freiesmagazin-2014-10-contest/src/
make
cd ../bots

for f in Bock Burow Maraun StaudingerP Wagenfuehr dummybot; do
  make -C $f
done

# Demel
# Abhängigkeit von Maven
cd Demel/
mvn install && cp target/HaraldTronBot-1.0.jar bin/
cd -

# Knof
make -C Knof/src

# Rochefort
cd Rochefort
./compile.sh
cd -

# Scharm
cd Scharm
mvn package
cd -

# Schmidhuber
# Abhängigkeit von Maven
cd Schmidhuber
./compile.sh
cd -

# StaudingerC
# nicht zu tun :-)


# 4. Zusammenfassung der Ergebnisse

echo
echo "/-----------------\\"
echo "| Zusammenfassung |"
echo "\\-----------------/"

if test -x ../game.bin; then
  echo "Spielserver: in Ordnung"
else
  echo "Spielserver: fehlgeschlagen"
fi


if test -x Bock/bot; then
  echo "Bock:        in Ordnung"
else
  echo "Bock:        fehlgeschlagen"
fi

if test -x Burow/FalkBot; then
  echo "Burow:       in Ordnung"
else
  echo "Burow:       fehlgeschlagen"
fi

if test -x Demel/bin/bot; then
  echo "Demel:       in Ordnung"
else
  echo "Demel:       fehlgeschlagen"
  echo "             Ist maven installiert?"
fi

if test -x Knof/src/tronbot; then
  echo "Knof:        in Ordnung"
else
  echo "Knof:        fehlgeschlagen"
  echo "             Vielleicht ist die Version vom Compiler g++ zu alt (< 4.8)"
fi

if test -x Maraun/bot; then
  echo "Maraun:      in Ordnung"
else
  echo "Maraun:      fehlgeschlagen"
fi

if test -x Rochefort/edrbot.jar; then
  echo "Rochefort:   in Ordnung"
else
  echo "Rochefort:   fehlgeschlagen"
fi

if test -f Scharm/target/FM14BFBot-0.6-jar-with-dependencies.jar; then
  echo "Scharm:      in Ordnung"
else
  echo "Scharm:      fehlgeschlagen"
fi

if test -x Schmidhuber/build/Bot.jar -a -d Schmidhuber/build/lasvegasbot -a $(stat -c %s Schmidhuber/build/Bot.jar) -ge 1000; then
  echo "Schmidhuber: in Ordnung"
else
  echo "Schmidhuber: fehlgeschlagen"
  echo "             Ist java 8 installiert und wird verwendet (javac -version)?"
fi

if test -x StaudingerC/bot.py; then
  echo "StaudingerC: in Ordnung"
  which python >/dev/null || echo "              Python scheint nicht installiert zu sein."
else
  # kann nicht vorkommen, ist ein python-Skript
  echo "StaudingerC: fehlgeschlagen"
fi

if test -f StaudingerP/tron/MyBot.class; then
  echo "StaudingerP: in Ordnung"
else
  echo "StaudingerP: fehlgeschlagen"
fi

if test -x Wagenfuehr/bot; then
  echo "Wagenfuehr:  in Ordnung"
else
  echo "Wagenfuehr:  fehlgeschlagen"
fi

if test -x dummybot/bot; then
  echo "dummybot:    in Ordnung"
else
  echo "dummybot:    fehlgeschlagen"
fi



cd ../..
