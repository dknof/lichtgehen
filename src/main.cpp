/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "constants.h"

#include "klassen/getopt.h"

#include "spielraster/spielraster.h"
#include "spieler/bot.h"
#include "spieler/mensch.h"
#include "ui/ui.h"
#include "spiel/spiel.h"
#include "ui/gtkmm/spielraster.h"

#include <string>
using std::string;
#include <sstream>
#include <algorithm>
#include <unistd.h>

std::ostream* cdebug_ = new std::ostringstream;
unique_ptr<UI> ui;
Status status = Status::PROGRAMMSTART;

#define ZEIT_MESSEN

#ifdef ZEIT_MESSEN
#include <chrono>
#endif

std::map<string, int> einstellungen;

int main_wettbewerb(int& argc, char* argv[]);
int main_eigenes_spiel(int& argc, char* argv[]);

/**
 ** Hauptfunktion
 **
 ** @param     argc   Anzahl der Argumente (wird ignoriert)
 ** @param     argv   Argumente (wird ignoriert)
 **
 ** @return    0
 **
 ** @version   2014-10-25
 **/
int
main(int argc, char* argv[])
{
cdebug_ = &cerr;
//cdebug_ = &cout;

#ifdef USE_EIGENES_SPIEL
return main_eigenes_spiel(argc, argv);
#else // Wettbewerb
cdebug_ = new std::ostringstream;
//cdebug_ = &cout;
return main_wettbewerb(argc, argv);
#endif
} // int main()

/**
 ** Hauptfunktion für ein eigenes Spiel (mit eigenen Spielern)
 **
 ** @param     argc   Anzahl der Argumente (-> GUI)
 ** @param     argv   Argumente (-> GUI)
 **
 ** @return    success/failure
 **
 ** @version   2015-03-30
 **
 ** @todo      Die Kollisionsabfrage zählt die Runden nicht genau (treffen sich zwei Spieler schafft der erste einen Schritt mehr), so reicht es aber zum Testen aus.
 **/
int
main_eigenes_spiel(int& argc, char* argv[])
{
  ::status = Status::PROGRAMMSTART;
  //cdebug_ = new std::ostringstream;
  // UI erzeugen
#ifdef USE_UI_GTKMM
  ::ui = UI::create("gtkmm", argc, argv);
#else
  //::ui = UI::create("none", argc, argv);
  ::ui = UI::create("cout", argc, argv);
  //::ui = UI::create("cerr", argc, argv);
#endif

  // Spiel erzeugen
  Spiel spiel;

  { // Kommandozeilenargumente durchgehen
    GetOpt::Option option;
    int spieler_num = -1; // Nummer des Spielers, der als Argument angegeben wird. -1 heißt erst einmal das Spielraster.
    while (option =
           GetOpt::getopt(argc, argv,
                          {{"hilfe",   'h',  GetOpt::Syntax::BOOL},
                          {"help",     '?',  GetOpt::Syntax::BOOL},
                          {"version",  'v',  GetOpt::Syntax::BOOL},
                          //{"debug",    '\0', GetOpt::Syntax::BOOL},
                          {"license",  'L',  GetOpt::Syntax::BOOL},
                          {"defines",  '\0', GetOpt::Syntax::BOOL},
                          //{"name",  'n', GetOpt::Syntax::BSTRING},
                          //{"ui",       'u',  GetOpt::Syntax::BSTRING},
                          {"1",        '1',  GetOpt::Syntax::BOOL},
                          {"2",        '2',  GetOpt::Syntax::BOOL},
                          {"3",        '3',  GetOpt::Syntax::BOOL},
                          {"4",        '4',  GetOpt::Syntax::BOOL},
                          {"5",        '5',  GetOpt::Syntax::BOOL},
                          {"6",        '6',  GetOpt::Syntax::BOOL},
                          {"7",        '7',  GetOpt::Syntax::BOOL},
                          {"8",        '8',  GetOpt::Syntax::BOOL},
                          {"9",        '9',  GetOpt::Syntax::BOOL},
                          {"runden",   'r',  GetOpt::Syntax::UNSIGNED},
                          }) // { }
          ) {

      if (option.fail()) {
        // Fehlbedienung
        cerr << argv[0] << "\n"
          << "Falsche Bedienung\n"
          << option.error() << " " << option.value_string() << "\n"
          << "Für Hilfe: '" << argv[0] << " --hilfe'"
          << endl;
        return EXIT_FAILURE;
      } // if (option.fail())

      if (option.name() == "hilfe") {
        // Hilfe ausgeben
#include "hilfe.string"

        cout << hilfe_string << endl;
        ::status = Status::PROGRAMMENDE;
      } else if (option.name() == "version") {
        // output of the version
        cout << "lichtgehen \n"
          << "Kompiliert: " << __DATE__ << ", " << __TIME__ << '\n'
          << "Kompiler: "
#ifdef __GNUG__
          << "g++"
#endif
          << '\n'
#ifdef __VERSION__
          << "  version: " << __VERSION__ << '\n'
#endif
#ifdef __cplusplus
          << "  C++ version: " << __cplusplus << '\n'
#endif
          ;
        ::status = Status::PROGRAMMENDE;
      } else if (option.name() == "license") {
        // output of the license (GPL)
#include "gpl.string"
        cout << "Lichtgehen -- Lizenz:\n\n"
          << gpl_string
          << endl;
        ::status = Status::PROGRAMMENDE;
      } else if (option.name() == "defines") {
        cout << "Lichtgehen Definitionen:\n";
#ifdef USE_EINGABE
        cout << "  USE_EINGABE        = " << USE_EINGABE << '\n';
#endif
#ifdef USE_EINGABE
        cout << "  USE_EIGENES_SPIEL  = " << USE_EIGENES_SPIEL << '\n';
#endif
#ifdef USE_THREADS
        cout << "  USE_THREADS  = " << USE_THREADS << "\n";
#endif
        cout << "\n";
        cout << "Lichtgehen Module:\n";
#ifdef USE_UI_TEXT
        cout << "  USE_UI_TEXT  = " << USE_UI_TEXT << '\n';
#endif
#ifdef USE_UI_GTKMM
        cout << "  USE_UI_GTKMM = " << USE_UI_GTKMM << '\n';
#endif
        ::status = Status::PROGRAMMENDE;
      } else if (option.name() == "debug") {
        // ToDo
      } else if (option.name() == "ui") {
        // ToDo
#if 0
        delete selected_ui;
        selected_ui = NULL;
        if (option.value_string() == "none")
          selected_ui = UI::new_(UI_TYPE::DUMMY);
#ifdef USE_UI_TEXT
        else if (option.value_string() == "text")
          selected_ui = UI::new_(UI_TYPE::TEXT);
#endif
#ifdef USE_UI_GTKMM
        else if (option.value_string() == "gtkmm")
          selected_ui = UI::new_(UI_TYPE::GTKMM_DOKO);
#endif
        else {
          cerr << "ui '" << option.value_string() << "' not implemented.\n"
            << "exiting";
          return EXIT_FAILURE;
        } // if (option.value_string() == "...")
#endif

      } else if (option.name() == "name") {
        // ToDo

      } else if (option.name() == "runden") {
        // nur 'runden' Runden machen, dann aufhören
        einstellungen["Runden"] = option.value(GetOpt::Option::UNSIGNED);
      } else if (option.name() == "1") {
        einstellungen["Runden"] = 1;
      } else if (option.name() == "2") {
        einstellungen["Runden"] = 2;
      } else if (option.name() == "3") {
        einstellungen["Runden"] = 3;
      } else if (option.name() == "4") {
        einstellungen["Runden"] = 4;
      } else if (option.name() == "5") {
        einstellungen["Runden"] = 5;
      } else if (option.name() == "6") {
        einstellungen["Runden"] = 6;
      } else if (option.name() == "7") {
        einstellungen["Runden"] = 7;
      } else if (option.name() == "8") {
        einstellungen["Runden"] = 8;
      } else if (option.name() == "9") {
        einstellungen["Runden"] = 9;

      } else if (option.name().empty()) {
        // Erst das Spielraster, dann die Spieler angeben
        if (spieler_num == -1) {
          // Das Spielraster wird angegeben
          spiel.setze_spielraster(option.value_string());
        } else {
          spiel.setze_spieler(spieler_num, option.value_string());
        }
        spieler_num += 1;
        if (   (option.value_string() == "Mensch") 
            || (spieler_num == spiel.spieler_anzahl()))
          // Fertig mit der Initialisierung
          ::status = Status::SPIEL;

      } else { // if (option.name() == ...)
        cerr << "Option '" << option.name() << "' unbekannt!" << endl;
        exit(EXIT_FAILURE);
      } // if (option.name() == ...)
    } // while (getopt())
    if (::status == Status::PROGRAMMENDE)
      return EXIT_SUCCESS;
    if (spieler_num < 0) {
      // kein Spielraster angegeben
      spiel.setze_spielraster("Einfach");
      spiel.setze_spieler(0, "Mensch");
    }
  } // Kommandozeilenargumente durchgehen

  while (::status != Status::PROGRAMMENDE) {
    if (   (::status == Status::PROGRAMMSTART)
        || (::status == Status::SPIELENDE))
      ::status = Status::SPIELINITIALISIERUNG;
    // Spiel initializieren
    spiel.initialisieren();

    if (::status != Status::PROGRAMMENDE) {
      ::status = Status::SPIEL;
      // Spiel spielen
      spiel.spielen();
    }

    if (::status != Status::PROGRAMMENDE) {
      ::status = Status::SPIELENDE;
#ifdef UI_GTKMM
      UI_Gtkmm::speicher(spiel.spielraster(), "Spielraster.pdf");
      UI_Gtkmm::speicher_verlauf(spiel.spielraster(), "Spielraster_Verlauf.pdf");
      //  UI_Gtkmm::speicher(spielraster, "Spielraster.png");
#endif
      // Spiel beenden
      spiel.beenden();
    }
  } // while (::status != STATUS::PROGRAMMENDE)

  ::status = Status::PROGRAMMENDE;
  return EXIT_SUCCESS;
} // int main_eigenes_spiel(int argc, char* argv[])


/**
 ** Hauptroutine für den Wettbewerb
 **
 ** @param     argc   Anzahl der Argumente (-> GUI)
 ** @param     argv   Argumente (-> GUI)
 **
 ** @return    success/failure
 **
 ** @version   2014-11-08
 **/
int
main_wettbewerb(int& argc, char* argv[])
{
  Spielraster spielraster;
#ifdef USE_UI_GTKMM
  auto ui = UI::create("gtkmm", argc, argv);
#else
  auto ui = UI::create("none", argc, argv);
  //auto ui = UI::create("cout", argc, argv);
  //auto ui = UI::create("cerr", argc, argv);
#endif
  ui = UI::create("none", argc, argv);
#ifdef USE_EINGABE
  Mensch spieler(*ui);
#else
  Bot spieler;
  spieler.setze_strategie(Strategie::create({"Tiefensuche",
                                            "in größten Raum",
                                            "zum größten Einflussgebiet",
                                            "Gegner verfolgen",
                                            "vorwärts 0.7",
                                            "links 0.7",
                                            }));
#endif

  string zeile;
  while (cin.good()) {
    std::getline(cin, zeile);
    //cdebug << "> " << zeile << '\n';
    std::istringstream istr(zeile);

    if (zeile.compare( 0, 15, "GAMEBOARDSTART ") == 0) {
      // GAMEBOARDSTART N,M - Es wird ein neues Spielbrett mit X Spalten und Y Zeilen übertragen. Endet mit GAMEBOARDEND
      auto text = zeile + "\n";
      do {
        std::getline(cin, zeile);
        text += zeile + "\n";
      } while (cin.good()
               && zeile != "GAMEBOARDEND") ;
      std::istringstream istr(text);
      spielraster.einlesen(istr);
#ifdef VERALTET
      spieler.spiel_startet(spielraster);
#endif

    } else if (zeile.compare( 0, 4, "SET ") == 0) {
      // SET P - Eigene Spielernummer festlegen
      spieler.setze_nummer(std::stoi(string(zeile, 4)) - 1);

    } else if (zeile.compare( 0, 4, "POS ") == 0) {
      // POS P N,M DIR - Position N,M des Spielers P auf dem Spielbrett festlegen, wobei der Spieler in Richtung DIR = (NORTH|EAST|SOUTH|WEST) schaut.
      spielraster.setze_spieler(istr);

    } else if (zeile.compare( 0, 6, "ROUND ") == 0) {
      // ROUND R - Runde R beginnt.  Das Spielbrett beginnt links oben bei (1,1).  Nach diesem Befehl erwartet der Server eine Antwort vom Client.

#ifdef VERALTET
      if (spielraster.runde() == 1)
        ui->spiel_startet(spielraster);
#endif
      ui->runde(spielraster.runde());
      if (spielraster.spieler_im_spiel(spieler.nummer()))
        cout << spieler.bewegung() << '\n';

    } else if (zeile.compare( 0, 4, "OUT ") == 0) {
      // OUT P - Spieler P ist ausgeschieden.

      spielraster.entferne_spieler(std::stoi(string(zeile, 4)) - 1);
    } else if (zeile == "END") {
      // END - Das Spiel ist zu Ende.
      ui->spiel_endet();
      break;

    } else if (zeile.compare(0, 5, "(II) )")) {
      cout << zeile << endl;
    } else {
      cerr << "Unbekannte Befehlszeile:\n" << zeile << '\n';
      break;
    } // if (zeile == …)
  } // while (true)

#ifdef UI_GTKMM
  UI_Gtkmm::speicher(spielraster, "Spielraster.pdf");
  UI_Gtkmm::speicher_verlauf(spielraster, "Spielraster_Verlauf.pdf");
  //UI_Gtkmm::speicher(spielraster, "Spielraster.png");
#endif

  //usleep(20*1000);
  //cdebug << "___\n" << spielraster << endl;
  //usleep(20*1000);

  return EXIT_SUCCESS;
} // int main_wettbewerb()


/*
   Kommandos vom Spieler an den Server:

   RIGHT  – Dreht den Spieler um 90 Grad nach rechts.
   LEFT   – Dreht den Spieler um 90 Grad nach links.
   AHEAD  – Lässt die Ausrichtung des Spielers, wie sie ist.
   */

