/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef STRATEGIE_TIEFENSUCHE_H
#define STRATEGIE_TIEFENSUCHE_H

#include "taktiken.h"

namespace StrategieNS {
  /** Tiefensuche, die spezielle Tiefensuche wird abhängig vom Status (alleine im Raum, Endspiel) gewählt
   **/
  class Tiefensuche : public Taktiken {
    enum SpielStatus {
      NORMAL = 0,
      LETZTE_FELDER = 0b01, // nur noch wenige Felder, kann auf Tiefensuche umsteigen
      EIGENER_RAUM = 0b10, // alleine im Raum, muss diesen nur noch gut ausfüllen
    };
    public:
    // Konstruktor
    Tiefensuche();
    // Konstruktor mit einer Taktik
    Tiefensuche(string name);
    // Konstruktor mit mehreren Taktiken
    Tiefensuche(std::initializer_list<string> name);

    // Destruktor
    ~Tiefensuche() override;

    // die Strategie ausgeben
    ostream& ausgeben(ostream& ostr) const override;

    // Die Bewegungsrichtung nach der Strategie ermitteln
    Bewegungsrichtung bewegung(Spielraster const& spielraster,
                               int spieler_nummer) override;

    protected:
    // teste auf Änderung des Spielstatus
    void teste_spielstatus(Spielraster const& spielraster, int spieler_nummer);

    protected:
    // Der Spielstatus
    int spielstatus = NORMAL;
  }; // class Tiefensuche : public Taktiken
} // namespace StrategieNS

#endif // #ifndef STRATEGIE_TIEFENSUCHE_H
