/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef STRATEGIE_ZWEI_H
#define STRATEGIE_ZWEI_H

#include "taktiken.h"

namespace StrategieNS {
/** Basisklasse für Strategieen
 ** Eine Strategie ist eine Sammlung von Taktiken.
 **
 ** @todo   Zeiger durch Zeigerklasse ersetzen
 **/
class Zwei : public Taktiken {
  enum SpielStatus {
    NORMAL = 0,
    LETZTE_FELDER   = 0b001, // nur noch wenige Felder, kann auf Tiefensuche umsteigen
    EIGENER_RAUM    = 0b010, // alleine im Raum, muss diesen nur noch gut ausfüllen
    DICHT_AM_GEGNER = 0b100, // dicht am Gegner
  };
  public:
    // Konstruktor
    Zwei();

    // Destruktor
    ~Zwei() override;

    // die Strategie ausgeben
    ostream& ausgeben(ostream& ostr) const override;

    // Die Bewegungsrichtung nach der Strategie ermitteln
    Bewegungsrichtung bewegung(Spielraster const& spielraster,
                               int spieler_nummer) override;

  protected:
    // teste auf Änderung des Spielstatus
    void teste_spielstatus(Spielraster const& spielraster,
                           int spieler_nummer);

  protected:
    // Der Spielstatus
    int spielstatus = -1;
}; // class Zwei : public Taktiken
} // namespace StrategieNS

#endif // #ifndef STRATEGIE_ZWEI_H
