/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "zwei.h"

namespace StrategieNS {

  /**
   ** Standardkonstruktor
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2015-03-31
   **/
  Zwei::Zwei() :
    Taktiken{}
  {
    this->setze_name("Strategie Zwei", "schnell zum Gegner, zum Spielende auf Tiefensuche wechseln");
  } // Zwei::Zwei()

  /**
   ** Standarddestruktor
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2015-03-31
   **/
  Zwei::~Zwei()
  { }

  /**
   ** Die Strategie auf ostr ausgeben
   ** 
   ** @param     ostr   Ausgabestrom
   **
   ** @return    Ausgabestrom
   **
   ** @version   2015-03-31
   **/
  ostream& 
    Zwei::ausgeben(ostream& ostr) const
    {
      this->Taktiken::ausgeben(ostr);
      if (this->spielstatus & SpielStatus::EIGENER_RAUM)
        ostr << "Eigener Raum\n";
      if (this->spielstatus & SpielStatus::LETZTE_FELDER)
        ostr << "Letzte Felder\n";
      if (this->spielstatus & SpielStatus::DICHT_AM_GEGNER)
        ostr << "Dicht am Gegner\n";
      return ostr;
    } // ostream& Zwei::ausgeben(ostream& ostr) const

  /**
   ** Endspiel prüfen
   ** Die Bewegungsrichtung nach den Taktiken ermitteln
   ** 
   ** @param     spielraster   das Spielraster
   ** @param     spieler_nummer  die Nummer des Spielers
   **
   ** @return    -
   **
   ** @version   2015-03-31
   **/
  Bewegungsrichtung
    Zwei::bewegung(Spielraster const& spielraster,
                                  int const spieler_nummer)
    {
      this->teste_spielstatus(spielraster, spieler_nummer);
      return this->Taktiken::bewegung(spielraster, spieler_nummer);
    } // Bewegungsrichtung Zwei::bewegung(Spielraster spielraster, int spieler_nummer)

  /**
   ** Teste die Änderung des Spielstatus
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2015-03-31
   **/
  void
    Zwei::teste_spielstatus(Spielraster const& spielraster,
                            int const spieler_nummer)
    {
      auto neuer_status = 0;

      auto const rauminfo = spielraster.rauminfo(spieler_nummer);

      // Teste, auf letzte Felder
      if (rauminfo.groesse <= 2 + 5) {
        CLOG << spieler_nummer << ": letzte Felder\n";
        neuer_status |= LETZTE_FELDER;
      }

      // Teste, auf eigenen Raum
      if (rauminfo.spieler_anz == 0) {
        CLOG << spieler_nummer << ": eigener Raum\n";
        neuer_status |= EIGENER_RAUM;
      }

      for (int s = 0; s < spielraster.spieler_anz(); ++s) {
        if (s == spieler_nummer)
          continue;
        auto const n = spielraster.kuerzeste_entfernung(spieler_nummer, s);
        if ((n > 0) && (n < 3)) {
          CLOG << spieler_nummer << ": dicht am Gegner\n";
          neuer_status |= DICHT_AM_GEGNER;
          break;
        }
      } // for (s)

      // Prüfe, ob sich der Status geändert hat
      if (neuer_status == this->spielstatus)
        return ;

      // Die Tiefensuche ist nur für zwei Spieler implementiert
      if (   (neuer_status == LETZTE_FELDER)
          && (rauminfo.spieler_anz > 1) )
        return ;

      this->taktiken.clear();
      this->hinzufuegen(Taktik::create("Tot"));
      this->hinzufuegen(Taktik::create("Einziger Weg"));

      if (neuer_status == 0) {
        this->hinzufuegen(Taktik::create("Tiefensuche"));
        this->hinzufuegen(Taktik::create("zum größten Einflussgebiet"));
      } else if (neuer_status == EIGENER_RAUM) {
        this->hinzufuegen(Taktik::create("Rechteck ausfüllen"));
        this->hinzufuegen(Taktik::create("Raum ausfüllen"));
      } else if (neuer_status == (LETZTE_FELDER | EIGENER_RAUM)) {
        this->hinzufuegen(Taktik::create("Raum ausfüllen (Tiefensuche)"));
      } else if (neuer_status & LETZTE_FELDER) {
        this->hinzufuegen(Taktik::create("Tiefensuche"));
      } else if (neuer_status == DICHT_AM_GEGNER) {
        this->hinzufuegen(Taktik::create("in größten Raum"));
        this->hinzufuegen(Taktik::create("zum größten Einflussgebiet"));
      } else {
        cerr << "Unbekannter neuer Status für die Strategie zwei: '" << neuer_status << "'\n";
      }

      this->spielstatus = neuer_status;

      return ;
    } // void Zwei::teste_spielstatus(Spielraster spielraster, int spieler_nummer)

} // namespace StrategieNS
