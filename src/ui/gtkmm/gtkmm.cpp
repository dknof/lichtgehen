/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "gtkmm.h"

#include "hauptfenster.h"
#include "hilfe.h"
#include "ueber.h"
#include <gtkmm/main.h>

namespace UI_Gtkmm {
  /**
   ** Konstruktor
   ** 
   ** @param     argc   Anzahl Kommandozeilenargumente
   ** @param     argv   Kommandozeilenargumente
   **
   ** @return    -
   **
   ** @version   2014-11-20
   **/
  UI_Gtkmm::UI_Gtkmm(int& argc, char* argv[]) :
    main{make_unique<Gtk::Main>(argc, argv)},
    hauptfenster{make_unique<Hauptfenster>(*this)},
    hilfe{make_unique<Hilfe>()},
    ueber{make_unique<Ueber>()}
  {
    this->hilfe->set_transient_for(*this->hauptfenster);
    this->ueber->set_transient_for(*this->hauptfenster);

    // @todo: Einen zweiten Thread eröffnen, um die GUI darzustellen.
    // Der Thread wird benötigt, da ein Thread auf cin wartet.
  } // UI_Gtkmm::UI_Gtkmm(int argc, char* argv[])

  /**
   ** Destruktor
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2014-11-20
   **/
  UI_Gtkmm::~UI_Gtkmm()
  { }


  /**
   ** Das Spielraster wurde geändert
   ** 
   ** @param     spielraster   das neue Spielraster
   **
   ** @return    -
   **
   ** @version   2015-02-23
   **/
  void
    UI_Gtkmm::spielraster_geaendert(Spielraster const& spielraster)
    {
      this->hauptfenster->spielraster_geaendert(spielraster);
    } // void UI_Gtkmm::spielraster_geaendert(Spielraster const& spielraster)

  /**
   ** Ein Spiel wird initialisiert
   ** 
   ** @param     spiel   Spiel
   **
   ** @return    -
   **
   ** @version   2015-02-26
   **/
  void
    UI_Gtkmm::spiel_initialisieren(Spiel& spiel)
    {
      this->UI::spiel_initialisieren(spiel);
      this->hauptfenster->present();
      this->hauptfenster->spiel_initialisieren();
      while (   (::status == Status::SPIELINITIALISIERUNG)
             && this->hauptfenster->is_visible())
        Gtk::Main::iteration();

      if (!this->hauptfenster->is_visible())
        ::status = Status::PROGRAMMENDE;

      return;
    } // void UI_Gtkmm::spiel_initialisieren(Spiel& spiel)

  /**
   ** Ein Spiel startet
   ** 
   ** @param     spiel   Spiel, das startet
   **
   ** @return    -
   **
   ** @version   2015-03-02
   **/
  void
    UI_Gtkmm::spiel_startet(Spiel const& spiel)
    {
      this->hauptfenster->spiel_startet();
      return;
    } // void UI_Gtkmm::spiel_startet(Spiel const& spiel)

  /**
   ** Eine neue Runde startet
   ** 
   ** @param     n   Nummer der Runde
   **
   ** @return    -
   **
   ** @version   2014-11-20
   **/
  void
    UI_Gtkmm::runde(int const n)
    {
      this->hauptfenster->runde(n);
      return;
    } // void UI_Gtkmm::runde(int n)

  /**
   ** Das Spiel ist zuende
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2014-11-20
   **/
  void
    UI_Gtkmm::spiel_endet()
    {
      this->hauptfenster->spiel_endet();
      while (   (::status == Status::SPIELENDE)
             && this->hauptfenster->is_visible())
        Gtk::Main::iteration();

      if (!this->hauptfenster->is_visible())
        ::status = Status::PROGRAMMENDE;

      return;
    } // void UI_Gtkmm::spiel_endet()

  /**
   ** gibt die nächste Richtung für die Bewegung zurück
   ** 
   ** @param     -
   **
   ** @return    nächste eingegebene Richtung
   **
   ** @version   2015-08-05
   **/
  Richtung
    UI_Gtkmm::naechste_richtung()
    { 
      while (   this->schwebende_richtungen.empty()
             && this->hauptfenster->is_visible()) {
        this->main->iteration();
        usleep(10000);
      };
      if (!this->hauptfenster->is_visible()) {
        if (this->schwebende_richtungen.empty()) {
          this->schwebende_richtungen.push_back(Richtung::NORDEN);
          this->schwebende_richtungen.push_back(Richtung::OSTEN);
          this->schwebende_richtungen.push_back(Richtung::SUEDEN);
          this->schwebende_richtungen.push_back(Richtung::WESTEN);
        }
      }
      auto const r = this->schwebende_richtungen.front();
      this->schwebende_richtungen.pop_front();
      return r;
    } // Richtung UI_Gtkmm::naechste_richtung()

} // namespace UI_Gtkmm
