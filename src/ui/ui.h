/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef UI_H
#define UI_H

#include "constants.h"
#include "../spielraster/richtung.h"
class Spiel;
class Spielraster;

class UI;
extern unique_ptr<UI> ui;

/** Die UI
 **/
class UI {
  public:
    // Ersteller
    static unique_ptr<UI> create(string text, int& argc, char* argv[]);

  public:
    // Konstruktor
    UI();
    // Destruktor
    virtual ~UI();

    // das Spiel
    Spiel const& spiel() const;
    // das Spiel
    Spiel& spiel();
    // das Spielraster
    Spielraster const& spielraster() const;

    //
    // Informationen über Änderungen
    //

    // Das Spielraster hat sich verändert
    virtual void spielraster_geaendert(Spielraster const& spielraster);

    //
    // Spielverlauf
    //

    // das Spiel wird initialisirt
    virtual void spiel_initialisieren(Spiel& spiel);
    // das Spiel startet
    virtual void spiel_startet(Spiel const& spiel);
    // neue Runde
    virtual void runde(int n);
    // das Spiel ist zuende
    virtual void spiel_endet();

    //
    // Rückgabe
    //

    // gibt die nächste Richtung (Benutzereingabe) zurück
    virtual Richtung naechste_richtung();

  private:
    // initializiere die UI
    void init();

  private:
    // Das Spiel
    Spiel* spiel_ = nullptr;
}; // class UI

#endif // #ifndef UI_H
