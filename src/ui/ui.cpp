/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "ui.h"

#include "text.h"
#ifdef USE_UI_GTKMM
#include "gtkmm/gtkmm.h"
#endif

#include "../spiel/spiel.h"
#include "../spielraster/spielraster.h"

/**
 ** Konstruktor
 ** 
 ** @param     argc   Anzahl Kommandozeilenargumente
 ** @param     argv   Kommandozeilenargumente
 **
 ** @return    -
 **
 ** @version   2014-11-20
 **/
unique_ptr<UI>
UI::create(string const name, int& argc, char* argv[])
{
  if (name == "none")
    return make_unique<UI>();
  else if (name == "text")
    return make_unique<UI_Text>(cout);
  else if (name == "cout")
    return make_unique<UI_Text>(cout);
  else if (name == "cerr")
    return make_unique<UI_Text>(cerr);
#ifdef USE_UI_GTKMM
  else if (name == "gtkmm")
    return make_unique<UI_Gtkmm::UI_Gtkmm>(argc, argv);
#endif
  else
    return nullptr;
} // static unique_ptr<UI> UI::create(string const name, int& argc, char* argv[])

/**
 ** Konstruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2014-11-20
 **/
UI::UI()
{ }

/**
 ** Destruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2014-11-20
 **/
UI::~UI()
{ }

/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    das Spiel
 **
 ** @version   2015-02-23
 **/
Spiel const&
UI::spiel() const
{
  return *this->spiel_;
} // Spiel const& UI::spiel() const

/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    das Spiel
 **
 ** @version   2015-02-23
 **/
Spiel&
UI::spiel()
{
  return *this->spiel_;
} // Spiel& UI::spiel()

/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    das Spielraster
 **
 ** @version   2015-02-23
 **/
Spielraster const&
UI::spielraster() const
{
  return this->spiel().spielraster();
} // Spielraster const& UI::spielraster() const

/**
 ** Das Spielraster wurde geändert
 ** 
 ** @param     spielraster   das neue Spielraster
 **
 ** @return    -
 **
 ** @version   2015-02-23
 **/
void
UI::spielraster_geaendert(Spielraster const& spielraster)
{ }

/**
 ** Ein Spiel wird initialisiert
 ** 
 ** @param     spiel    das Spiel
 **
 ** @return    -
 **
 ** @version   2015-02-23
 **/
void
UI::spiel_initialisieren(Spiel& spiel)
{
  this->spiel_ = &spiel;
  return;
} // void UI::spiel_initialisieren(Spiel& spiel)

/**
 ** Ein Spiel startet
 ** 
 ** @param     spiel    das Spiel
 **
 ** @return    -
 **
 ** @version   2015-02-23
 **/
void
UI::spiel_startet(Spiel const& spiel)
{ }

/**
 ** Eine neue Runde startet
 ** 
 ** @param     n   Nummer der Runde
 **
 ** @return    -
 **
 ** @version   2014-11-20
 **/
void
UI::runde(int const n)
{ }

/**
 ** Das Spiel ist zuende
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2014-11-20
 **/
void
UI::spiel_endet()
{ }

/**
 ** gibt die nächste Richtung für die Bewegung zurück
 ** 
 ** @param     -
 **
 ** @return    alle Richtungen abwechselnd
 **
 ** @version   2014-11-20
 **/
Richtung
UI::naechste_richtung()
{
  // alle durchgehen, falls eine belegt is
  static int n = 0;
  n += 1;
  n %= ::richtungen.size();
  return ::richtungen[n];
} // Richtung UI::naechste_richtung()
