/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef SPIEL_H
#define SPIEL_H

#include "constants.h"

#include "../spielraster/spielraster.h"
#include "../spieler/spieler.h"

/** Ein Spiel
 ** Das Spiel enthält nur die übergeordnete Verwaltung, der Spielverlauf ist in Spielraster enthalten.
 **/
class Spiel {
  public:
    // Konstruktor
    Spiel();
    // Destruktor
    ~Spiel();

    // Spiel ausgeben
    ostream& ausgeben(ostream& ostr) const;

    // der Status
    Status status() const;

    // setze das Spielraster
    void setze_spielraster(string pfad, string name = "");
    // das Spielraster
    Spielraster const& spielraster() const;
    // das Spielraster
    Spielraster& spielraster();
    // Anzahl der Spieler an das Spielraster anpassen
    void spieler_anzahl_anpassen();

    // setze den Spieler
    void setze_spieler(int n, string typ);
    // Spieleranzahl
    int spieler_anzahl() const;
    // der Spieler
    Spieler const& spieler(int n) const;
    // der Spieler
    Spieler& spieler(int n);

    // die Runde
    int runde() const;

    // das Spiel starten und durchspielen
    void initialisieren();
    void spielen();
    void beenden();

  private:
    // Das Spielraster
    unique_ptr<Spielraster> spielraster_ = nullptr;
    // Die Spieler
    vector<unique_ptr<Spieler>> spieler_;

    // die Runde
    int runde_ = 0;
}; // class Spiel

// Spiel ausgeben
ostream& operator<<(ostream& ostr, Spiel const& spiel);

#endif // #ifndef SPIEL_H
