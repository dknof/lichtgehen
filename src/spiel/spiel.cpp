/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "spiel.h"
#include "../spieler/bot.h"
#include "../ui/ui.h"
#include <algorithm>
#include <fstream>

/**
 ** Standardkonstruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
Spiel::Spiel()
{
  this->spielraster_ = std::make_unique<Spielraster>(10, 10);
  this->spielraster().setze_spieler(0, SpielerPosition(Position(0, 0), Richtung::OSTEN));
  //this->spielraster().setze_spieler(1, SpielerPosition(Position(5, 5), Richtung::NORDEN));
  this->spieler_anzahl_anpassen();
} // Spiel::Spiel()

/**
 ** Destruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
Spiel::~Spiel()
{ }

/**
 ** Das Spiel auf ostr ausgeben
 ** 
 ** @param     ostr   Ausgabestrom
 ** @param     spiel  Spiel zum ausgeben
 **
 ** @return    Ausgabestrom
 **
 ** @version   2015-03-01
 **/
ostream&
operator<<(ostream& ostr, Spiel const& spiel)
{ return spiel.ausgeben(ostr); }

/**
 ** Gibt das Spiel auf dem Ausgabestrom aus
 ** 
 ** @param     ostr   Ausgabestrom
 **
 ** @return    -
 **
 ** @version   2015-03-01
 **/
ostream&
Spiel::ausgeben(ostream& ostr) const
{
  ostr << "Spielraster\n";
  ostr << this->spielraster();
  ostr << "Spieler: " << this->spieler_anzahl() << '\n';
  for (auto const& s : this->spieler_)
    ostr << s->nummer() << ' ' << s->name() << '\n';

  return ostr;
} // ostream& Spiel::ausgeben(ostream& ostr) const

/**
 ** setzt das Spielraster
 ** 
 ** @param     pfad   Pfad zum Spielraster
 ** @param     name   Name des Spielrasters
 **
 ** @return    -
 **
 ** @version   2015-03-30
 **/
void
Spiel::setze_spielraster(string const pfad, string const name)
{
  if (name == "") {
    // Vordefinierte Raster testen
    auto r = Raster::vordefiniert.find(pfad);
    if (r != Raster::vordefiniert.end()) {
      this->setze_spielraster(r->second, pfad);
      return ;
    }
  }

  std::ifstream istr(pfad);
  if (!istr.good()) {
    cerr << "Fehler beim Öffnen des Rasters '" << pfad << "'.\n";
    return ;
  }
  auto spielraster = std::make_unique<Spielraster>(); // das Spielraster
  spielraster->setze_name(name);
  spielraster->einlesen(istr);

  { // Positionen der Spieler lesen
    while (istr.peek() && istr.good()) {
      spielraster->setze_spieler(istr);
#if 0
      break; // Test: nur ein Spieler
#endif
      while (isspace(istr.peek()))
        istr.get();
    } // while (istr.good())
  }

  if (!istr.eof())
    return ;

  this->spielraster_ = std::move(spielraster);
  this->spieler_.resize(std::max(this->spieler_.size(),
                                 static_cast<size_t>(this->spielraster().spieler_anz())));
  // Spieler erzeugen
  for (auto& s : this->spieler_) {
    if (!s)
      s = Spieler::create("Bot");
  } // for (s : this->spieler_)
  for (size_t i = 0; i < this->spieler_.size(); ++i)
    this->spieler(i).setze_nummer(i);

  ::ui->spielraster_geaendert(this->spielraster());

  return ;
} // void Spiel::setze_spielraster(string pfad)

/**
 ** Passt die Anzahl der Spieler an das Spielraster an
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-26
 **/
void
Spiel::spieler_anzahl_anpassen()
{
  size_t const n = this->spielraster().spieler_anz();

  // fehlende Spieler erzeugen
  for (auto i = this->spieler_.size(); i < n; ++i)
    this->setze_spieler(i, "Bot");

  // zuviele Spieler entfernen
  this->spieler_.resize(std::max(this->spieler_.size(), n));

  return ;
} // void Spiel::spieler_anzahl_anpassen()


/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    das Spielraster
 **
 ** @version   2015-02-22
 **/
Spielraster const&
Spiel::spielraster() const
{
  return *this->spielraster_;
} // Spielraster Spiel::spielraster() const

/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    das Spielraster
 **
 ** @version   2015-02-22
 **/
Spielraster&
Spiel::spielraster()
{
  return *this->spielraster_;
} // Spielraster Spiel::spielraster()

/**
 ** setzt den Spieler
 ** 
 ** @param     n     Spielernummer
 ** @param     typ   Typ des Spielers
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
void
Spiel::setze_spieler(int const n, string const typ)
{
  auto spieler = Spieler::create(typ);
  if (!spieler)
    return ;
  if (typ == "Bot") {
    auto strategie = Strategie::create("zwei");
    dynamic_cast<Bot*>(spieler.get())->setze_strategie(std::move(strategie));
  }
  if (static_cast<size_t>(n) >= this->spieler_.size())
    this->spieler_.resize(n+1);
  this->spieler_[n] = std::move(spieler);
  this->spieler_[n]->setze_nummer(n);
  return ;
} // void Spiel::setze_spieler(int const n, string const typ)

/**
 ** -> Rückgabe
 ** 
 ** @param     -
 **
 ** @return    die Anzahl der Spieler
 **
 ** @version   2015-02-22
 **/
int
Spiel::spieler_anzahl() const
{
  return this->spieler_.size();
} // int Spiel::spieler_anzahl() const

/**
 ** -> Rückgabe
 ** 
 ** @param     n   Nummer des Spielers
 **
 ** @return    der n-te Spieler
 **
 ** @version   2015-02-22
 **/
Spieler const&
Spiel::spieler(int const n) const
{
  return *this->spieler_[n];
} // Spieler Spiel::spieler(int n) const

/**
 ** -> Rückgabe
 ** 
 ** @param     n   Nummer des Spielers
 **
 ** @return    der n-te Spieler
 **
 ** @version   2015-02-22
 **/
Spieler&
Spiel::spieler(int const n)
{
  return *this->spieler_[n];
} // Spieler Spiel::spieler(int n)

/**
 ** das Spiel starten und durchspielen
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
int
Spiel::runde() const
{
  return this->runde_;
} // int Spiel::runde() const

/**
 ** das Spiel initialisieren
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
void
Spiel::initialisieren()
{
  //this->setze_spieler(0, "Bock");
  //this->setze_spieler(1, "Bock");
  //this->setze_spieler(1, "Knof");
  if (::status != Status::PROGRAMMSTART)
    ::ui->spiel_initialisieren(*this);
  return ;
} // void Spiel::initialisieren()

/**
 ** das Spiel starten und durchspielen
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
void
Spiel::spielen()
{
  Spielraster& spielraster = this->spielraster();

  this->runde_ = 0;
  auto naechster_schritt = vector<Bewegungsrichtung>(spielraster.spieler_anz());
  for (auto& s : this->spieler_)
    s->spiel_startet(*this);
  ::ui->spiel_startet(*this);

  for (auto& s : this->spieler_)
    cdebug << s->name() << endl;

  while (spielraster.spieler_im_spiel()) {
    for (auto& s : this->spieler_)
      s->runde(this->runde());
    ::ui->runde(this->runde());

    cdebug << "Runde " << this->runde() << '\n';
    if (::einstellungen.find("Runden") != ::einstellungen.end())
      if (this->runde() >= ::einstellungen["Runden"])
        break;

    this->runde_ += 1;
    //cdebug << spielraster << '\n';
    //usleep(10000);
    // Schritte abfragen
    for (int b = 0; b < spielraster.spieler_anz(); ++b) {
      if (spielraster.position(b)) {
        //cdebug << b << " erreichbare Raumgröße: " << spielraster.raumgroesse_erreichbar(spielraster.position(b)) << '\n';
#ifdef ZEIT_MESSEN
        auto const zeit_start = std::chrono::system_clock::now();
#endif
        naechster_schritt[b] = this->spieler(b).bewegung();
#ifdef ZEIT_MESSEN
        std::chrono::duration<double> const dauer
          = std::chrono::system_clock::now() - zeit_start;
        if (dauer.count() > 0.9 * ZEITBESCHRAENKUNG) {
          cout << b << ": " << dauer.count() << " Sekunden\n";
        }
#endif

      }
    }

    // Schritte gehen
    auto pos_neu = vector<Position>(spielraster.spieler_anz());
    for (int b = 0; b < spielraster.spieler_anz(); ++b) {
      if (spielraster.position(b))
        pos_neu[b] = spielraster.position(b) + naechster_schritt[b];
    }
    for (int b = 0; b < spielraster.spieler_anz(); ++b) {
      if (!spielraster.position(b))
        continue;
      if (std::count(begin(pos_neu), end(pos_neu), pos_neu[b]) > 1) {
        spielraster.entferne_spieler(b);
        spielraster.belege(pos_neu[b]);
      } else {
        spielraster.bewege_spieler(b, naechster_schritt[b]);
      }
    } // for (b)
#if 0
    if (spielraster.spieler_im_spiel() == 1)
      break;
#endif
  } // while (spielraster.spieler_im_spiel())

  return;
} // void Spiel::spielen()

/**
 ** das Spiel beenden
 ** 
 ** @param     -
 **
 ** @return    -
 **
 ** @version   2015-02-22
 **/
void
Spiel::beenden()
{
  for (auto& s : this->spieler_)
    s->spiel_endet();
  ::ui->spiel_endet();

  return ;
} // void Spiel::beenden()
