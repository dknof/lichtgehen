/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef TAKTIK_RECHTECK_AUSFUELLEN_H
#define TAKTIK_RECHTECK_AUSFUELLEN_H

#include "taktik.h"

namespace TaktikNS {
/** Taktik RechteckAusfuellen
 ** Ein Rechteck möglichst gut ausfüllen. Es wird vorausgesetzt, dass der Bot alleine in dem Raum ist.
 **/
class RechteckAusfuellen : public Taktik {
  public:
    // Konstruktor
    RechteckAusfuellen();

    // gibt das Ergebnis der Taktik zurück (ob sie greift und die Richtung)
    Ergebnis ergebnis(Spielraster const& spielraster,
                      int spieler_nummer) override;

  private:
    // gibt zurück, ob es vorwärts Richtung Wand geht, mit 'richtung' im Raum
    bool an_wand(Spielraster const& spielraster,
                 SpielerPosition pos,
                 Bewegungsrichtung richtung);

}; // class RechteckAusfuellen : public Taktik

} // namespace TaktikNS

#endif // #ifndef TAKTIK_RECHTECK_AUSFUELLEN_H
