/*
   lichtgehen

   Copyright (C) 2014 by Diether Knof

   This file is part of lichtgehen.

   Lichtgehen is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Lichtgehen is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Diese Datei ist Teil von lichtgehen.

   Lichtgehen ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Lichtgehen wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "rechteck_ausfuellen.h"

namespace TaktikNS {
  /**
   ** Standardkonstruktor
   ** 
   ** @param     -
   **
   ** @return    -
   **
   ** @version   2015-03-27
   **/
  RechteckAusfuellen::RechteckAusfuellen() :
    Taktik{"Rechteck ausfüllen", "Das Rechteck möglichst gut ausfüllen"}
  { }

  /**
   ** -> Rückgabe
   ** Wenn der Bereich zu einem Rechteck gehört, von unten aus auffüllen.
   ** Es muss kein ganzes Rechteck sein sondern die unteren drei Reihen reichen aus:
   ** |          |
   ** |          |
   ** |     <-----
   ** ############
   ** 
   ** @param     spielraster   das Spielraster
   ** @param     spieler_nummer   die Nummer des Spielers
   **
   ** @return    Am Rand entlang
   **
   ** @version   2015-03-27
   **/
  Taktik::Ergebnis
    RechteckAusfuellen::ergebnis(Spielraster const& spielraster, int const spieler_nummer)
    {
      auto const pos = spielraster.position(spieler_nummer);
      if (!spielraster(pos + Bewegungsrichtung::VORWAERTS)) {
        if (this->an_wand(spielraster, pos, Bewegungsrichtung::RECHTS))
          return Bewegungsrichtung::VORWAERTS;
        if (this->an_wand(spielraster, pos, Bewegungsrichtung::LINKS))
          return Bewegungsrichtung::VORWAERTS;
      } // if (!spielraster(pos + Bewegungsrichtung::VORWAERTS))

      auto const posr = SpielerPosition(pos, pos.richtung() + Bewegungsrichtung::RECHTS);
      if (!spielraster(posr + Bewegungsrichtung::VORWAERTS)) {
        if (this->an_wand(spielraster, posr, Bewegungsrichtung::RECHTS))
          return Bewegungsrichtung::RECHTS;
        if (this->an_wand(spielraster, posr, Bewegungsrichtung::LINKS))
          return Bewegungsrichtung::RECHTS;
      } // if (!spielraster(posr + Bewegungsrichtung::VORWAERTS))

      auto const posl = SpielerPosition(pos, pos.richtung() + Bewegungsrichtung::LINKS);
      if (!spielraster(posl + Bewegungsrichtung::VORWAERTS)) {
        if (this->an_wand(spielraster, posl, Bewegungsrichtung::RECHTS))
          return Bewegungsrichtung::LINKS;
        if (this->an_wand(spielraster, posl, Bewegungsrichtung::LINKS))
          return Bewegungsrichtung::LINKS;
      } // if (!spielraster(posl + Bewegungsrichtung::VORWAERTS))

      return false;
    } // Taktik::Ergebnis RechteckAusfuellen::ergebnis(Spielraster const& spielraster, int spieler_nummer)


  /**
   ** -> Rückgabe
   ** 
   ** @param     spielraster   das Spielraster
   ** @param     pos           Position
   ** @param     richtung      Richtung zur Wand
   **
   ** @return    ob es vorwärts Richtung Wand geht, mit 'richtung' im Raum
   **
   ** @version   2015-03-28
   **/
  bool
    RechteckAusfuellen::an_wand(Spielraster const& spielraster,
                                SpielerPosition pos,
                                Bewegungsrichtung const richtung)
    {
      // richtung = rechts: links muss ein Wand sein, rechts immer zwei Felder frei

      do { // while (!spielraster(pos));
        if (!(   !spielraster(pos + richtung)
              && !spielraster(pos + richtung + Bewegungsrichtung::VORWAERTS)
              && spielraster(pos + (richtung == Bewegungsrichtung::RECHTS
                                    ? Bewegungsrichtung::LINKS
                                    : Bewegungsrichtung::RECHTS)))) {
          return false;
        }
        pos += Bewegungsrichtung::VORWAERTS;
      } while (!spielraster(pos));
      // geradeaus bis zur Wand gelaufen
      if (   spielraster(pos + richtung)
          && spielraster(pos + richtung + Bewegungsrichtung::VORWAERTS)) {
        return true;
      }
      return false;
    } // bool RechteckAusfuellen::an_wand(Spielraster const& spielraster, pos pos, Bewegungsrichtung richtung)
} // namespace TaktikNS
