/*
   Copyright (C) 2002 – 2015 by Diether Knof

   Getopt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Getopt is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Getopt ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Getopt wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#include "getopt.h"

#include <string.h>
#include <stdarg.h>

namespace GetOpt {

  // das n-te Argument entfernen
  void remove_argument(int& argc, char* argv[], int n = 1);
  // remove 'i' argument characters at position  '[n1][n2]'
  // default: 'program -asdf' -> 'program -sdf'
  void remove_argument_chars(int& argc, char* argv[], unsigned i = 1);

  /**
   ** gibt das nächste Argument zurück und entfernt es aus argc und argv
   ** 
   ** @param     argc     Anzahl der Argumente (wird verändert)
   ** @param     argv     die Argumente (werden verändert)
   ** @param     syntax   Syntax der Argumente
   **
   ** @return    das nächste Argument
   **
   ** @author    Dr. Diether Knof
   ** 
   ** @version   2015-03-29
   **/
  Option
    getopt(int& argc, char* argv[],
           std::vector<Syntax> const& syntax)
    {
      Option option;

      if (argc == 0) {
        // error -- there must be one argument
        option.error_v = Option::Error::NO_ARGUMENT;
        return option;
      } else if ((argc == 1)
                 || ((argc == 2)
                     && (strncmp(argv[1], "--", 3) == 0)
                    )
                ) {
        // no remaining argument (or only '--')
        option.error_v = Option::Error::NO_OPTION;
        return option;
      } else { // if (argc > 1)
        if ((argv[1][0] != '-')
            || (argv[1][1] == '\0')) {
          // a sole string
          option.error_v = Option::Error::OK;
          option.name_v = "";
          option.type_v = Syntax::BSTRING;
          option.value_string_v = argv[1];

          remove_argument(argc, argv);

          return option;
        } // if (argv[1][0] != '-')
        else if (strncmp(argv[1], "--", 3) == 0) {
          // a sole string on the second position
          option.error_v = Option::Error::OK;
          option.name_v = "";
          option.type_v = Syntax::BSTRING;
          option.value_string_v = argv[2];

          remove_argument(argc, argv, 2);

          return option;
        } // if (strncmp(argv[1], "--", 3) == 0)
        else if (strncmp(argv[1], "--", 2) == 0) {
          // long option
          option.value_string_v = argv[1];
          option.error_v = Option::Error::UNKNOWN_OPTION;
          // ***DK use Iterator
          for (unsigned n = 0; n < syntax.size(); n++) {
            // search the option
            if (strncmp(argv[1] + 2, syntax[n].name.c_str(),
                        syntax[n].name.length()) == 0) {

              option.type_v = syntax[n].type;
              option.name_v = syntax[n].name;

              if (option.type() == Syntax::BOOL) {
                if (argv[1][2 + syntax[n].name.size()] == '\0') {
                  option.error_v = Option::Error::OK;
                  option.value_v.b = true;
                  remove_argument(argc, argv);

                  return option;
                } // if (argument = '--name'

              } else { // if !(option.type() == Syntax::BOOL)

                if (argv[1][2 + syntax[n].name.size()] == '\0') {
                  // only the name of the option is in this argument,
                  // the value (if any) is in the next
                  remove_argument(argc, argv);

                  // test, whether there is a remaining argument
                  if (argc == 1) {
                    option.error_v = Option::Error::NO_ARGUMENT;

                    return option;

                  } else { // if !(argc == 1)
                    // the value of the option is now the argument number 1
                    (option.value_string_v += " ") += argv[1];
                    option.value_set(argv[1]);
                    if (option.error() == Option::Error::OK)
                      remove_argument(argc, argv);

                    return option;

                  } // if !(argc == 1)
                } // if (value in next argument)
                else if (argv[1][2 + syntax[n].name.size()] == '=') {

                  option.value_set(argv[1] + 2 + syntax[n].name.length() + 1);
                  if (option.error() == Option::Error::OK)
                    remove_argument(argc, argv);

                  return option;

                } // if (argument = '--name=value')

              } // if !(option.type() == Syntax::BOOL)

            } // if (strncmp(argv[1] + 2, syntax, ) == 0)
          } // for (n < syntax.size())

        } else { // if !(long option)

          // long option
          option.value_string_v = argv[1];
          option.error_v = Option::Error::UNKNOWN_OPTION;

          // short option
          // ***DK use Iterator
          for (unsigned n = 0; n < syntax.size(); n++) {
            // search the option
            if (argv[1][1] == syntax[n].short_name) {

              option.type_v = syntax[n].type;
              option.name_v = syntax[n].name;

              if (option.type() == Syntax::BOOL) {
                // BOOL
                option.error_v = Option::Error::OK;
                option.value_v.b = true;
                remove_argument_chars(argc, argv);
                if (argv[1][1] == '\0') // argument is empty
                  remove_argument(argc, argv);

                return option;

              } else { // if !(option.type() == Syntax::BOOL)

                if (argv[1][2] == '\0') {
                  // the value is in the next argument
                  remove_argument(argc, argv);
                  (option.value_string_v += " ") += argv[1];
                  option.value_set(argv[1]);
                  if (option.error() == Option::Error::OK)
                    remove_argument(argc, argv);

                  return option;

                } else { // if !(argv[1][2] == '\0')

                  // the value is in this argument
                  if ((option.type() == Syntax::CHAR)
                      && !(argv[1][2] == '=')) {
                    // CHAR
                    option.error_v = Option::Error::OK;
                    option.value_v.c = argv[1][2];
                    remove_argument_chars(argc, argv, 2);
                    if (argv[1][1] == '\0') // argument is empty
                      remove_argument(argc, argv);

                    return option;

                  } else { // if !(option.type() == Syntax::BOOL)
                    // an '=' is ignored
                    option.value_set(argv[1] + 2 +
                                     (argv[1][2] == '=' ? 1 : 0));
                    if (option.error() == Option::Error::OK)
                      remove_argument(argc, argv);

                    return option;

                  } // if !(option.type() == Syntax::BOOL)

                } // if (value in this argument)

              } // if !(option.type() == Syntax::BOOL)
            } // if (argv[1][1] == syntax[n].short_name)
          } // for (n < syntax.size())

        } // if (short option)

      } // if !(argc > 1)

      return option;
    } // Option getopt(int& argc, char* argv[], std::vector<Syntax> const& syntax)

  /**
   ** Entfernt das n-te Argument
   ** 
   ** @param     argc    Anzahl der Argumente (wird verändert)
   ** @param     argv    die Argumente (werden verändert)
   ** @param     n       Nummer des Arguments zum Entfernen
   **
   ** @return    -
   **
   ** @author    Dr. Diether Knof
   ** 
   ** @version   2015-03-29
   **/
  void
    remove_argument(int& argc, char* argv[], int const n)
    {
      if (n <= 0)
        return; // Fehler: unerlaubter Wert
      if (n >= argc)
        return; // Fehler: Zuviele Argumente zu entfernen

      for (auto i = n; i < argc - 1; ++i)
        argv[i] = argv[i + 1];

      argc -= 1;

      return;
    } // void remove_argument(int& argc, char* argv[], int const n = 1)

  /**
   ** ein Argument mit angegebener Länge
   ** Standard: 'program -asdf' -> 'program -sdf'
   ** 
   ** @param     argc   Anzahl der Argumente (wird verändert)
   ** @param     argv   die Argumente (werden verändert)
   ** @param     i      Anzahl der Zeichen, die entfernt werden sollen
   **
   ** @return    -
   **
   ** @author    Dr. Diether Knof
   ** 
   ** @version   2015-03-29
   **/
  void
    remove_argument_chars(int& argc, char* argv[], unsigned const i)
    {
      if (argc == 0)
        return; // Fehler: kein Argument mehr da
      if (argv[1][0] != '-')
        return; // Fehler: kein Argument


      if (i + 1 > strlen(argv[1])) {
        return; // Fehler: zu viele Zeichen zum entfernen
      }

#ifdef OUTDATED
      // 2015-03-29
      strcpy(argv[n1] + n2, argv[n1] + n2 + i);
#else
      argv[1] += i;
      argv[1][0] = '-';
      if (argv[1][1] == 0) {
        remove_argument(argc, argv);
      }
#endif

      return;
    } // void remove_argument_chars(int& argc, char* argv[], i = 1)
} // namespace GetOpt
