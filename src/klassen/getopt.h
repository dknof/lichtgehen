/*
   Copyright (C) 2002 – 2015 by Diether Knof

   Getopt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   Software Foundation, either version 3 of the License, or
   your option) any later version.

   Getopt is distributed in the hope that it will be useful,
   but without any warranty; without even the implied warranty of
   merchantability or fitness for a particular purpose.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

   Getopt ist Freie Software: Sie können es unter den Bedingungen
   der GNU General Public License, wie von der Free Software Foundation,
   Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   veröffentlichten Version, weiterverbreiten und/oder modifizieren.

   Getopt wird in der Hoffnung, dass es nützlich sein wird, aber
   ohne jede Gewährleistung, bereitgestellt; sogar ohne die implizite
   Gewährleistung der Marktfähigkeit oder Eignung für einen bestimmten Zweck.
   Siehe die GNU General Public License für weitere Details.

   Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   */

#ifndef GETOPT_HEADER
#define GETOPT_HEADER

#include <string>
#include <vector>

namespace GetOpt {

  struct Syntax {
    // Typ des Arguments
    enum Type {
      BOOL,
      INT,
      UNSIGNED,
      DOUBLE,
      CHAR,
      BSTRING,
      END
    }; // enum Type

    std::string name; // (lange) Name einer Option (Pflicht)
    char short_name; // Kurzform einer Option (optional)
    Type type; // Typ der Option
  }; // struct Syntax

  class Option {
    friend Option getopt(int& argc, char* argv[],
			 std::vector<Syntax> const& syntax);
    public:
    // Fehlercode
    enum class Error {
      OK,
      NO_OPTION,
      UNKNOWN_OPTION,
      FALSE_ARGUMENT,
      NO_ARGUMENT,
      UNKNOWN_ERROR
    }; // enum Error
    // verschiedene Optionstypen
    enum TypeBool { BOOL };
    enum TypeInt { INT = BOOL + 1 };
    enum TypeUnsigned { UNSIGNED = INT + 1 };
    enum TypeDouble { DOUBLE = UNSIGNED + 1 };
    enum TypeChar { CHAR = DOUBLE + 1 };
    // could not use 'STRING', 'B' stands for 'basic'
    enum TypeString { BSTRING = CHAR + 1 };
    public:
    // Konstruktor
    Option();
    // Konstruktor
    Option(Option const& option);
    // setting equal
    Option& operator=(Option const& option);

    // desctuctor
    ~Option();

    // the error code
    Error error() const;
    // wether the parsing has failed
    bool fail() const;
    // whether an option could be parsed (with or without an error)
    operator bool() const;

    // the name of the option
    std::string const& name() const;
    // the Type of the option
    Syntax::Type type() const;

    // the boolean value
    bool value(TypeBool) const;
    // the int value
    int value(TypeInt) const;
    // the unsigned value
    unsigned value(TypeUnsigned) const;
    // the double value
    double value(TypeDouble) const;
    // the char value
    char value(TypeChar) const;
    // the string value
    std::string const& value(TypeString) const;
    // the string value
    std::string const& value_string() const;

    // set the value
    Error value_set(char const* argv);

    // ***  private:
    public:
    Error error_v; // the errorcode
    std::string name_v; // the name of the option
    Syntax::Type type_v; // the type
    union {
      bool b;
      int i;
      unsigned u;
      double d;
      char c;
    } value_v; // the value of the option
    std::string value_string_v; // could not put this in the union
  }; // class Option

  // get the next option
  Option getopt(int& argc, char* argv[], std::vector<Syntax> const& syntax);

} // namepsace GetOpt

// Fehler ausgeben
std::ostream& operator<<(std::ostream& ostr, GetOpt::Option::Error const error);


#endif
